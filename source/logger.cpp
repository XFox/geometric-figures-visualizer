#define _CRT_SECURE_NO_WARNINGS

#include <fstream>
#include <chrono>
#include <iomanip>
#include <ctime>

#include "logger.h"


void Logger::Log(ErrorObjects object, WarningLevel level, std::string message)
{
	std::ofstream file;
	file.open("error_log.txt", std::fstream::out | std::fstream::app);
	//Get time
	time_t currentTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

	file << '[' << std::put_time(std::localtime(&currentTime), "%Y-%m-%d %T") << "][";

	switch (level)
	{
	case Logger::WarningLevel::Info:
		file << "INFO";
		break;
	case Logger::WarningLevel::Warn:
		file << "WARN";
		break;
	case Logger::WarningLevel::Error:
		file << "ERROR";
		break;
	default:
		break;
	}

	file << "][";

	switch (object)
	{
	case ErrorObjects::ObjectShader:
		file << "SHADER";
		break;
	case ErrorObjects::ObjectShaderManager:
		file << "SHADER_MANAGER";
		break;
	case ErrorObjects::ObjectRenderer:
		file << "RENDERER";
		break;
	case ErrorObjects::ObjectWindowManager:
		file << "WINDOW_MANAGER";
		break;
	default:
		break;
	}

	file << "]: " << message << std::endl;

	file.close();
}
