#include <stdexcept>
#include <filesystem>
#include <set>

#include "shaderManager.h"
#include "logger.h"
ShaderManager::ShaderManager()
{
	std::set<std::string> files;
	std::string path = "shaders/";
	for (auto& entry : std::filesystem::directory_iterator(path))
	{
		std::string file = entry.path().filename().string();
		size_t pos = 0;
		if ((pos = file.find('.')) != std::string::npos)
		{
			file = file.substr(0, pos);
		}
		files.insert(file);
	}
	for (std::string shaderName : files)
	{
		AddShader(shaderName);
	}
}

void ShaderManager::AddShader(std::string shaderName)
{
	std::unique_ptr<Shader> shader = std::make_unique<Shader>(shaderName);
	library.insert(std::make_pair(shaderName, std::move(shader)));
}


const std::unique_ptr<Shader>& ShaderManager::GetShader(std::string shaderName)
{
	try
	{
		return library.at(shaderName);
	}
	catch (std::out_of_range e)
	{
		using namespace Logger;
		Log(ErrorObjects::ObjectShaderManager, WarningLevel::Error, "Shader named '" + shaderName + "' was not found in manager.");
		return nullptr;
	}
}
