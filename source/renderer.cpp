#include <cmath>
#include <cstdlib>
#include <chrono>

#include "glad.h"
#include "glm/vec3.hpp"

#include "figure.h"
#include "renderer.h"

void Renderer::FillFigures()
{
	figures.clear();
	using namespace FigureFactory;
	for (int i = 0; i < rand() % 10 + 2; i++) 
	{
		switch (rand() % 3) 
		{
		case 0:
			figures.emplace_back(CreateFigure(Figures::TypeTriangle));
			break;
		case 1:
			figures.emplace_back(CreateFigure(Figures::TypeQuad));
			break;
		case 2:
			figures.emplace_back(CreateFigure(Figures::TypeCircle));
			break;
		default:
			break;
		}
	}
}

const std::vector<std::unique_ptr<Figure>>& Renderer::GetFigures()
{
	return figures;
}

Renderer::Renderer()
{
	std::srand(time(0));
}

void BasicRenderer::DrawFigures()
{
	float offsetX = -1.5f * (float)figures.size();//������ ���� ��������� �� ������� + ��������� �/��� ����������, ������� ����� ������, ������ ���� ��� ������� � ���������� ������.
	float secondOffsetX;
	for (int i = 0; i < figures.size(); i++) 
	{
		const std::vector<glm::vec3>& temp = figures[i]->GetVertices();//����� �������� �����������, ���� ��������� const ... &
		secondOffsetX = (i + 1) * 3.0f - 1.5f;
		glPushMatrix();
		glTranslatef(offsetX + secondOffsetX, 0, 0);
		glBegin(GL_LINE_STRIP);
		glColor3f(1.0f, 1.0f, 1.0f);
		for (auto &vertex : temp)
		{
			glVertex2f(vertex.x, vertex.y);
		}
		glVertex2f(temp[0].x, temp[0].y);
		glEnd();
		glPopMatrix();
	}
}