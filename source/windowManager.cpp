#include "glad.h"
#include "windowManager.h"

bool WindowManager::InitWindow()
{
	if (!glfwInit()) return false;
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	window = glfwCreateWindow(800, 600, "Geometric Figures", 0, 0);
	if (!window) return false;
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, KeyCallback);
	glfwSetWindowUserPointer(window, this);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	double baseDim = 15;
	glOrtho(-baseDim * 1.33, baseDim * 1.33, -baseDim, baseDim, 0, 1);
	return true;
}

void WindowManager::RunMainLoop()
{
	while (!glfwWindowShouldClose(window)) 
	{
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		renderer->DrawFigures();
		glfwSwapBuffers(window);
		glfwWaitEvents();
	}
}

void WindowManager::SetNewRenderer(std::unique_ptr<Renderer> rend)
{
	if (renderer == nullptr)
	{
		renderer = std::move(rend);
	}
	else
	{
		renderer.swap(rend);
	}
}

void WindowManager::RecreateFigures()
{
	renderer->FillFigures();
}

WindowManager::~WindowManager()
{
	glfwTerminate();
}

void WindowManager::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	auto manager = static_cast<WindowManager*>(glfwGetWindowUserPointer(window));
	if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
	{
		manager->RecreateFigures();
	}
}
