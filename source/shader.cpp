#include <fstream>
#include <sstream>
#include <filesystem>

#include "shader.h"
#include "glad.h"
#include "logger.h"

bool Shader::CheckCompileErrors(GLuint shader, std::string type)
{
	std::ofstream fs;
	GLint success;
	GLchar infoLog[1024];
	if (type != "program")
	{
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(shader, 1024, NULL, infoLog);
			using namespace Logger;
			Log(ErrorObjects::ObjectShader, WarningLevel::Error, "SHADER_COMPILATION_ERROR of type: " + type + "------------" + infoLog);
			return true;
		}
	}
	else
	{
		glGetProgramiv(shader, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(shader, 1024, NULL, infoLog);
			using namespace Logger;
			Log(ErrorObjects::ObjectShader, WarningLevel::Error, "PROGRAM_LINKING_ERROR of type: " + type + "------------" + infoLog);
			return true;
		}
	}
	return false;
}

Shader::Shader(std::string shaderName)
{
	std::string shaderPath = "shaders/";
	std::string vertexShaderFileName = shaderName + ".vert";
	std::string fragmentShaderFileName = shaderName + ".frag";
	std::string geometryShaderFileName = shaderName + ".geom";

	std::string vertexCode = "";
	std::string geometryCode = "";
	std::string fragmentCode = "";

	std::stringstream stringStr;
	std::ifstream fs;

	const char* vShaderCode = nullptr;
	const char* gShaderCode = nullptr;
	const char* fShaderCode = nullptr;
	
	//Vertex shader read
	if (std::filesystem::exists(shaderPath + vertexShaderFileName))
	{
		fs.open(shaderPath + vertexShaderFileName);
		stringStr << fs.rdbuf();
		vertexCode = stringStr.str();
		fs.close();
		stringStr.str("");

		//Vertex shader compile
		vShaderCode = vertexCode.c_str();
		vertexShader = glCreateShader(GL_VERTEX_SHADER); // create
		glShaderSource(vertexShader, 1, &vShaderCode, 0); // link source
		glCompileShader(vertexShader); // compile

		if (CheckCompileErrors(vertexShader, "vertex"))
		{
			throw new std::exception("Failed to compile shader with ID " + vertexShader);
		}
	}
	else
	{
		throw new std::exception(("File " + shaderPath + vertexShaderFileName + " not found. This file is required.").c_str());
	}

	//Geometry shader read
	if (std::filesystem::exists(shaderPath + geometryShaderFileName))
	{
		fs.open(shaderPath + geometryShaderFileName);
		stringStr << fs.rdbuf();
		geometryCode = stringStr.str();
		fs.close();
		stringStr.str("");

		//Geometry shader compile
		gShaderCode = geometryCode.c_str();
		geometryShader = glCreateShader(GL_GEOMETRY_SHADER); // create
		glShaderSource(geometryShader, 1, &gShaderCode, 0); // link source
		glCompileShader(geometryShader); // compile

		if (CheckCompileErrors(geometryShader, "geometry"))
		{
			throw new std::exception("Failed to compile shader with ID " + geometryShader);
		}
	}

	//Fragment shader read
	if (std::filesystem::exists(shaderPath + fragmentShaderFileName))
	{
		fs.open(shaderPath + fragmentShaderFileName);
		stringStr << fs.rdbuf();
		fragmentCode = stringStr.str();
		fs.close();
		stringStr.str("");

		//Fragment shader compile
		fShaderCode = fragmentCode.c_str();
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER); // create
		glShaderSource(fragmentShader, 1, &fShaderCode, 0); // link source
		glCompileShader(fragmentShader); // compile

		if (CheckCompileErrors(vertexShader, "fragment"))
		{
			throw new std::exception("Failed to compile shader with ID " + fragmentShader);
		}
	}
	else
	{
		throw new std::exception(("File " + shaderPath + fragmentShaderFileName + " not found. This file is required.").c_str());
	}

	id = glCreateProgram();
	glAttachShader(id, vertexShader);
	if (gShaderCode != nullptr) glAttachShader(id, geometryShader);
	glAttachShader(id, fragmentShader);
	glBindAttribLocation(id, 0, "vertexPosition");
	glLinkProgram(id);

	if (CheckCompileErrors(id, "program"))
	{
		throw new std::exception("Failed to compile program with ID " + id);
	}
	glDeleteShader(vertexShader);
	if (gShaderCode != nullptr) glDeleteShader(geometryShader);
	glDeleteShader(fragmentShader);
}

const void Shader::Use()
{
	glUseProgram(id);
}
