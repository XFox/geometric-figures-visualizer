#include <cmath>

#include "figure.h"

Triangle::Triangle()
{
}

Triangle::Triangle(glm::vec3 targetPos) : Figure(targetPos)
{
}

void Triangle::FillVerts()
{
	vertices.emplace_back(.0f, 1.0f, .0f);
	vertices.emplace_back(-1.0f, -1.0f, .0f);
	vertices.emplace_back(1.0f, -1.0f, .0f);
}

Quad::Quad()
{
}

Quad::Quad(glm::vec3 targetPos) : Figure(targetPos)
{
}

void Quad::FillVerts()
{
	vertices.emplace_back(-1.0f, 1.0f, .0f);
	vertices.emplace_back(-1.0f, -1.0f, .0f);
	vertices.emplace_back(1.0f, -1.0f, .0f);
	vertices.emplace_back(1.0f, 1.0f, .0f);
}

Circle::Circle()
{
}

Circle::Circle(glm::vec3 targetPos) : Figure(targetPos)
{
}

void Circle::FillVerts()
{
	for (float i = .0f; i < 6.28f; i += 0.314f) //i=0,20
	{
		vertices.emplace_back(std::cos(i), std::sin(i), .0f);
	}
}

std::unique_ptr<Figure> FigureFactory::CreateFigure(Figures type)
{
	std::unique_ptr<Figure> fig;
	switch (type)
	{
	case Figures::TypeTriangle:
		fig = std::make_unique<Triangle>();
		break;
	case Figures::TypeQuad:
		fig = std::make_unique<Quad>();
		break;
	case Figures::TypeCircle:
		fig = std::make_unique<Circle>();
		break;
	}
	fig->FillVerts();
	return std::move(fig);
}
