#include "windowManager.h"
#include "renderer.h"
int main() 
{
	std::unique_ptr<WindowManager> manager = std::make_unique<WindowManager>();
	if (!manager->InitWindow()) return -1;
	manager->SetNewRenderer(std::move(std::make_unique<BasicRenderer>()));
	manager->RecreateFigures();
	manager->RunMainLoop();
	return 0;
}