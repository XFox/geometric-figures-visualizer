#pragma once
#include <string>

#include "glad.h"

class Shader
{
public:
	Shader(std::string shaderName);
	const void Use();

private:
	unsigned int id = 0;
	unsigned int vertexShader = 0;
	unsigned int geometryShader = 0;
	unsigned int fragmentShader = 0;

	bool CheckCompileErrors(GLuint shader, std::string type);
};