#pragma once
#include <string>

namespace Logger
{
	enum class ErrorObjects
	{
		ObjectShader,
		ObjectShaderManager,
		ObjectWindowManager,
		ObjectRenderer
	};
	enum class WarningLevel
	{
		Info,
		Warn,
		Error
	};
	void Log(ErrorObjects object, WarningLevel level, std::string message);
}