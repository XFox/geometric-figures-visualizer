#pragma once
#include <vector>
#include <memory>

#include "figure.h"

class Renderer 
{
public:
	void FillFigures();
	virtual void DrawFigures() = 0;
	const std::vector<std::unique_ptr<Figure>>& GetFigures();

protected:
	Renderer();

	std::vector<std::unique_ptr<Figure>> figures;
};
//������� ���������� ������� ����� ���� �� ��������� ��� ��������� �������. ���������� ���������.
class BasicRenderer : public Renderer
{
public:
	BasicRenderer() {}

	void DrawFigures() override;
};