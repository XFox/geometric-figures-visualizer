#pragma once
#include <vector>
#include <memory>

#include "glm/vec3.hpp"

class Figure
{
public:
	const std::vector<glm::vec3>& GetVertices() { return vertices; }
	void SetPosition(glm::vec3 targetPos) { pos = targetPos; }
	const glm::vec3& GetPosition() { return pos; }

	virtual void FillVerts() = 0;

protected:
	Figure() {}
	Figure(glm::vec3 targetPos) : pos(targetPos) {}


	glm::vec3 pos = {.0f, .0f, .0f};
	std::vector<glm::vec3> vertices;
};

//�������������� �����������
class Triangle : public Figure 
{
public:
	Triangle();
	Triangle(glm::vec3 targetPos);

	void FillVerts() override;

protected:
};

//�������
class Quad : public Figure 
{
public:
	Quad();
	Quad(glm::vec3 targetPos);

	void FillVerts() override;

protected:
};

//����
class Circle : public Figure 
{
public:
	Circle();
	Circle(glm::vec3 targetPos);

	void FillVerts() override;

protected:
};

//Figure factory �� �� �������, ��� ����������� FillVerts
namespace FigureFactory
{
	enum class Figures
	{
		TypeTriangle,
		TypeQuad,
		TypeCircle
	};

	std::unique_ptr<Figure> CreateFigure(Figures type);
}