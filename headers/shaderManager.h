#pragma once
#include <map>
#include <memory>

#include "shader.h"

class ShaderManager
{
public:
	//Must be called after glad init
	ShaderManager();
	const std::unique_ptr<Shader>& GetShader(std::string shaderName);
private:
	std::map<std::string, std::unique_ptr<Shader> > library;
	void AddShader(std::string shaderName);
};