#pragma once
#include <memory>

#include "GLFW/glfw3.h"
#include "renderer.h"

class WindowManager 
{
public:
	WindowManager() {}
	~WindowManager();

	bool InitWindow();
	void RunMainLoop();
	void SetNewRenderer(std::unique_ptr<Renderer> rend);
	void RecreateFigures();

private:
	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

	GLFWwindow* window = nullptr;
	std::unique_ptr<Renderer> renderer;
};